# ec2-devstack
Automatically setup devstack on EC2

You need to have prepared ~/.aws/credentials apropriately in order for
the AWS interaction through Boto3 to work.

Deploying an EC2 will incur a cost - always make sure to kill your instance
when it's no longer needed!
