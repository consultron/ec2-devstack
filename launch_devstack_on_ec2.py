#!/usr/bin/env python
#
# Copyright (C) 2017 Stefan K. Berg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# Inspired by:
# https://www.datadoghq.com/blog/install-openstack-in-two-commands/
# https://docs.openstack.org/developer/devstack/

import boto3
import sys
import os
import paramiko
import StringIO
import time
import subprocess


class ErrorRunningCommand(Exception):
  def __init__( self, rc ):
    self.rc = rc
    Exception.__init__(self, 'Got return code %d when running SSH command' % rc)

def delete_security_group():
  ec2 = boto3.resource('ec2')
  # Make a dictionary of all security groups
  sgs = list(ec2.security_groups.all())
  sgx={}
  for sg in sgs:
    sgx[sg.group_name] = sg.id

  try:
    security_group = ec2.SecurityGroup(sgx['devstack_sg'])
    security_group.delete()
  except Exception as e:
    print("Oups when deleting security group: " + str(e))

def create_security_group():
  ec2 = boto3.resource('ec2')

  try:
    mysg = ec2.create_security_group(GroupName="devstack_sg",Description='testme')
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=22,ToPort=22)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=80,ToPort=80)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=443,ToPort=443)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=3506,ToPort=3506)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=5000,ToPort=5000)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=5672,ToPort=5672)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=5900,ToPort=5999)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=6000,ToPort=6002)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=6080,ToPort=6082)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=8000,ToPort=8000)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=8003,ToPort=8003)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=8080,ToPort=8080)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=8386,ToPort=8386)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=8773,ToPort=8777)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=9191,ToPort=9191)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=9292,ToPort=9292)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=9696,ToPort=9696)
    mysg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=35357,ToPort=35357)

  except Exception as e:
    print("Exception in create_security_group: " + str(e))

def delete_keypair():
  ec2 = boto3.resource('ec2')

  keypair = ec2.KeyPair('devstack_keypair')
  keypair.delete()

def create_keypair():
  ec2 = boto3.resource('ec2')

  keypair = ec2.create_key_pair(KeyName='devstack_keypair')
  print "Writing key material to certificate.pem"
  f = open( 'certificate.pem', 'w' )
  f.write(keypair.key_material)
  f.close()
  os.chmod('certificate.pem',0600)

def terminate_instance():
  ec2 = boto3.resource('ec2')

  # List and kill all running instances
  instances = ec2.instances.filter(
    Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])
  for instance in instances:
    print(instance.id, instance.instance_type)
    for tag in instance.tags:
      if 'Name' in tag['Key']:
        if tag['Value'] == 'devstack':
          name = tag['Value']
          print("Stopping and terminating " + instance.id + " (" + name + ")" + " of type " + instance.instance_type)
          instance.terminate()
          print("Waiting for termination...")
          instance.wait_until_terminated()
          print("Terminated.")
          
          
def create_instance():
  ec2 = boto3.resource('ec2')

  instances = ec2.create_instances(
    ImageId='ami-1c45e273',
    InstanceType='m4.large',
    AdditionalInfo="devstack_instance",
    KeyName="devstack_keypair",
    SecurityGroups=['devstack_sg'],
    MinCount=1,
    MaxCount=1)
  instance = instances[0]

  print "Waiting for instance to boot..."
  instance.wait_until_running()


  # Reload the instance attributes
  instance.load()

  tag = instance.create_tags(
    DryRun=False,
    Tags=[
      {
        'Key': 'Name',
        'Value': 'devstack'
      },
    ]
  )
    
  f = open( 'connect.cmd', 'w' )
  f.write("ssh -i certificate.pem ubuntu@" + instance.public_dns_name + "\n")
  f.close()
  return instance.public_dns_name

def ssh_setup(host,user,certfile):
  f = open(certfile,'r')
  s = f.read()
  f.close()
  keyfile = StringIO.StringIO(s)
  mykey = paramiko.RSAKey.from_private_key(keyfile)

  #### BEGIN temporary fix for Crypto.Cipher.AES warning message
  # Borrowed with pride from http://uucode.com/blog/2015/02/20/workaround-for-ctr-mode-needs-counter-parameter-not-iv/

  import Crypto.Cipher.AES
  orig_new = Crypto.Cipher.AES.new
  def fixed_AES_new(key, *ls):
    if Crypto.Cipher.AES.MODE_CTR == ls[0]:
      ls = list(ls)
      ls[1] = ''
    return orig_new(key, *ls)

  Crypto.Cipher.AES.new = fixed_AES_new
  #### END temporary fix for Crypto.Cipher.AES warning message

  ssh = paramiko.SSHClient()
  ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

  ssh.connect(host,username=user,pkey=mykey)
  print "Connected"
  

  return ssh

def ssh_run_command(ssh, arg):
  tran = ssh.get_transport()
  chan = tran.open_session()
  chan.set_combine_stderr(True)
  chan.get_pty()
  f = chan.makefile()
  
  print('SSH: Sending ' + arg)
  chan.exec_command(arg)

  for line in iter(f.readline,""):
    print(line).rstrip('\r\n')

  rc = chan.recv_exit_status()
  if rc:
    raise ErrorRunningCommand(rc)


  return rc

terminate_instance()
delete_security_group()
create_security_group()
delete_keypair()
create_keypair()  

try:
  fqdn = create_instance()
  time.sleep(10)

  print("Attempting connection against " + fqdn)

  ssh = ssh_setup(fqdn,'ubuntu','certificate.pem')

  rc = ssh_run_command(ssh, 'sudo useradd -s /bin/bash -d /opt/stack -m stack')
  rc = ssh_run_command(ssh, 'echo "stack ALL=(ALL) NOPASSWD: ALL" > /tmp/stack.sudo')
  rc = ssh_run_command(ssh, 'sudo cp /tmp/stack.sudo /etc/sudoers.d/stack')
  rc = ssh_run_command(ssh, 'sudo cp -Rvp /home/ubuntu/.ssh /opt/stack')
  rc = ssh_run_command(ssh, 'sudo chown -R stack:stack /opt/stack/.ssh')

  ssh = ssh_setup(fqdn,'stack','certificate.pem')
  rc = ssh_run_command(ssh, 'git clone https://git.openstack.org/openstack-dev/devstack')
  rc = ssh_run_command(ssh, 'cd devstack; pwd')

  password = subprocess.check_output(['/usr/bin/pwgen','15','1']).rstrip('\r\n')

  rc = ssh_run_command(ssh, 'cd devstack; echo "[[local|localrc]]" > local.conf')
  rc = ssh_run_command(ssh, 'cd devstack; echo "ADMIN_PASSWORD=' + password + '" >> local.conf')
  rc = ssh_run_command(ssh, 'cd devstack; echo "DATABASE_PASSWORD=\$ADMIN_PASSWORD" >> local.conf')
  rc = ssh_run_command(ssh, 'cd devstack; echo "RABBIT_PASSWORD=\$ADMIN_PASSWORD" >> local.conf')
  rc = ssh_run_command(ssh, 'cd devstack; echo "SERVICE_PASSWORD=\$ADMIN_PASSWORD" >> local.conf')
  rc = ssh_run_command(ssh, 'cd devstack; cat local.conf')

  rc = ssh_run_command(ssh, 'cd devstack; ./stack.sh')
except ErrorRunningCommand, exc:
  print exc
  terminate_instance()
